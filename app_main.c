#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "cmsis_os2.h"
#include "iot_errno.h"
#include "ohos_init.h"
#include "app_log.h"
#include "app_task.h"
#include "app_led.h"
#include "app_oled.h"
#include "app_pwm.h"
#include "app_env.h"
#include "app_cloud.h"

static char *TAG = "app_main.c";

/* use code module start */
#define LED_STUDY_TASK 0
#define PWM_STUDY_TASK 0
#define CLOUD_STUDY_TASK 1
#define ENV_STUDY_TASK 1
#define OLED_STUDY_TASK 1
#define APP_MAIN_TASK 1
/* use code module end */

/* define led code start */
#define LED_TASK_STACK_SIZE (1024 * 4)
#define LED_TASK_PRIO 25
/* define led code end */

/* define pwm code start */
#define PWM_TASK_STACK_SIZE (1024 * 4)
#define PWM_TASK_PRIO 25
/* define pwm code end */

/* define env code start */
#define ENV_TASK_STACK_SIZE (1024 * 8)
#define ENV_TASK_PRIO 26
/* define env code end */

/* define oled code start */
#define OLED_TASK_STACK_SIZE (1024 * 8)
#define OLED_TASK_PRIO 23
/* define oled code end */

/* define cloud code start */
#define CLOUD_TASK_STACK_SIZE (1024 * 10)
#define CLOUD_TASK_PRIO 25
#define SENSOR_TASK_STACK_SIZE (1024 * 8)
#define SENSOR_TASK_PRIO 26
/* define cloud code end */

/* define app code start */
#define APP_TASK_STACK_SIZE (1024 * 8)
#define APP_TASK_PRIO 25
/* define app code end */

static void app_main(void)
{
    /* create mutex */
    create_mutex();

    /* start task set osThreadAttr_t */
    osThreadAttr_t attr;

    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;

    /* start create task */
#if APP_MAIN_TASK
    attr.name = "app_task";
    attr.priority = APP_TASK_PRIO;
    attr.stack_size = APP_TASK_STACK_SIZE;
    if (osThreadNew((osThreadFunc_t)app_task, NULL, &attr) == NULL)
    {
        MY_LOGI(TAG, "Failed to create AppTask!\n");
    }
    else
    {
        MY_LOGI(TAG, "Succeed to create AppTask!\n");
    }
#endif

#if LED_STUDY_TASK
    attr.name = "led_test_task";
    attr.priority = LED_TASK_PRIO;
    attr.stack_size = LED_TASK_STACK_SIZE;
    if (osThreadNew((osThreadFunc_t)led_test_task, NULL, &attr) == NULL)
    {
        MY_LOGI(TAG, "Failed to create LedTESTTask!\n");
    }
    else
    {
        MY_LOGI(TAG, "Succeed to create LedTESTTask!\n");
    }
#endif

#if PWM_STUDY_TASK
    attr.name = "pwm_test_task";
    attr.priority = PWM_TASK_PRIO;
    attr.stack_size = PWM_TASK_STACK_SIZE;
    if (osThreadNew((osThreadFunc_t)pwm_test_task, NULL, &attr) == NULL)
    {
        MY_LOGI(TAG, "Failed to create PWMTESTTask!\n");
    }
    else
    {
        MY_LOGI(TAG, "Succeed to create PWMTESTTask!\n");
    }
#endif

#if ENV_STUDY_TASK
    attr.name = "env_task";
    attr.priority = ENV_TASK_PRIO;
    attr.stack_size = ENV_TASK_STACK_SIZE;
    if (osThreadNew((osThreadFunc_t)env_task, NULL, &attr) == NULL)
    {
        MY_LOGI(TAG, "Failed to create EnvTask!\n");
    }
    else
    {
        MY_LOGI(TAG, "Succeed to create EnvTask!\n");
    }
#endif

#if OLED_STUDY_TASK
    attr.name = "oled_test_task";
    attr.priority = OLED_TASK_PRIO;
    attr.stack_size = OLED_TASK_STACK_SIZE;
    if (osThreadNew((osThreadFunc_t)oled_test_task, NULL, &attr) == NULL)
    {
        MY_LOGI(TAG, "Failed to create OLEDTESTTask!\n");
    }
    else
    {
        MY_LOGI(TAG, "Succeed to create OLEDTESTTask!\n");
    }
#endif

#if CLOUD_STUDY_TASK
    attr.name = "CloudMainTaskEntry";
    attr.stack_size = CLOUD_TASK_STACK_SIZE;
    attr.priority = CLOUD_TASK_PRIO;
    if (osThreadNew((osThreadFunc_t)CloudMainTaskEntry, NULL, &attr) == NULL)
    {
        MY_LOGI(TAG, "Faifan to create CloudMainTaskEntry!\n");
    }
    else
    {
        MY_LOGI(TAG, "Succeed to create CloudMainTaskEntry!\n");
    }

    attr.stack_size = SENSOR_TASK_STACK_SIZE;
    attr.priority = SENSOR_TASK_PRIO;
    attr.name = "SensorTaskEntry";
    if (osThreadNew((osThreadFunc_t)SensorTaskEntry, NULL, &attr) == NULL)
    {
        MY_LOGI(TAG, "Faifan to create SensorTaskEntry!\n");
    }
    else
    {
        MY_LOGI(TAG, "Succeed to create SensorTaskEntry!\n");
    }
#endif
}

APP_FEATURE_INIT(app_main);
