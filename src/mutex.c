#include "mutex.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>

const static char *TAG = "mutex";

osMutexId_t log_mutexId;
osMutexId_t env_mutexId;
osMutexId_t ctrl_mutexId;

/**
 * @brief create_mutex
 *
 */
void create_mutex(void)
{
    log_mutexId = osMutexNew(NULL);
    if (log_mutexId == NULL) {
        printf("[%s][I]Failed to create log Mutex!\n",TAG);
    }
    else{
        printf("[%s][I]Succeed to create log Mutex!\n",TAG);
    }

    env_mutexId = osMutexNew(NULL);
    if (env_mutexId == NULL) {
        printf("[%s][I]Failed to create env Mutex!\n",TAG);
    }
    else{
        printf("[%s][I]Succeed to create env Mutex!\n",TAG);
    }

    ctrl_mutexId = osMutexNew(NULL);
    if (ctrl_mutexId == NULL) {
        printf("[%s][I]Failed to create ctrl Mutex!\n",TAG);
    }
    else{
        printf("[%s][I]Succeed to create ctrl Mutex!\n",TAG);
    }
}


