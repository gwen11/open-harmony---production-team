#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio_ex.h"
#include "sht30_bh1750.h"
#include "app_env.h"
#include "app_log.h"
#include "app_pwm.h"
#include "app_led.h"
#include "iot_pwm.h"
#include "mutex.h"

const static char *TAG = "app_env.c";

#define TASK_DELAY_1S 100 /* os tick is 10ms */

#define Light_GPIO 5
#define Fan_GPIO 2
#define Fan_GPIO_FUNC IOT_GPIO_FUNC_GPIO_2_PWM2_OUT
#define DEBUG 1

env_ctrl_status app_env_ctrl_status; /* 控制状态结构体 */
env_data app_env_data;               /* 环境数据结构体 */

/**
 * @brief threshold_lag_ctrl
 * @note  -- 阈值滞后反馈和湿度异常开关除湿
 * @param uint16_t 湿度阈值
 * @param uint16_t 光照阈值
 * @param uint8_t  电机速度
 * @param env_data 环境数据
 * @param env_ctrl_status 控制状态
 */
static void threshold_lag_ctrl(uint16_t humi_threshold, uint16_t lux_threshold, uint8_t duty, env_data *data, env_ctrl_status *status)
{
    if (data->Humidity >= (humi_threshold + HYSTERESIS)) // 湿度异常
    {
        /* 当前湿度高于阈值加上滞后阈值 */
        status->fan = 1;          /* 赋值风扇状态 */
        pwm_ctrl(Fan_GPIO, duty); /* ON Fan */
        MY_LOGI(TAG, "Open Fan\n");
    }
    else if (data->Humidity < (humi_threshold - HYSTERESIS)) // 湿度正常
    {
        /* 当前湿度低于阈值减去滞后阈值 */
        status->fan = 0;      /* 赋值风扇状态 */
        IoTPwmStop(Fan_GPIO); /* OFF Fan */
        MY_LOGI(TAG, "Stop Fan\n");
    }

    if (data->Lux <= (lux_threshold + HYSTERESIS))
    {
        status->light = 1;          /* 赋值灯状态 */
        led_ctrl(Light_GPIO, 1, 1); /* ON Light */
    }
    else if (data->Lux >= (lux_threshold - HYSTERESIS))
    {
        status->light = 0;          /* 赋值灯状态 */
        led_ctrl(Light_GPIO, 0, 1); /* OFF Light */
    }
}

/**
 * @brief update_ctrl_status
 *
 * @param uint16_t humi_threshold
 * @param uint8_t duty
 * @param env_data
 * @param env_ctrl_status
 */
static void update_ctrl_status(uint16_t humi_threshold, uint16_t lux_threshold, uint8_t duty, env_data *data, env_ctrl_status *status)
{
    osMutexAcquire(ctrl_mutexId, osWaitForever); /* 获取控制状态互斥锁 */
    if (status->flag_priority == 0)
    {
        threshold_lag_ctrl(humi_threshold, lux_threshold, duty, data, status);/* 滞后控制 */
    }
    else
    {
    }

    osMutexRelease(ctrl_mutexId); /* 释放控制状态互斥锁 */
}

/**
 * @brief env_task
 *
 */
void env_task(void)
{
    int ret;
    ret = sht30_bh1750_init();
    if (ret != 0)
    {
        MY_LOGI(TAG, "sht30_bh1750 Init failed!\r\n");
        return;
    }

    led_init(Light_GPIO);
    MY_LOGI(TAG, "Init light!\r\n");
    pwmIO_init(Fan_GPIO, Fan_GPIO_FUNC);
    MY_LOGI(TAG, "Init motor!\r\n");

    /* test data define start */
    //float humi_read_data = 66.5, lux_read_data = 32.2, temp_read_data = 30.2;
    /* test data define end */

    while (1)
    {
        /* test data start */
        // humi_read_data += 4.2;
        // lux_read_data += 1;
        // temp_read_data += 1;
        // if (humi_read_data >= 90)
        // {
        //     humi_read_data = 30.3;
        // }
        // if (lux_read_data >= 60)
        // {
        //     lux_read_data = 22.2;
        // }
        // if (temp_read_data >= 40)
        // {
        //     temp_read_data = 25;
        // }
        // app_env_data.Lux = lux_read_data;
        // app_env_data.Humidity = humi_read_data;
        // app_env_data.Temperature = temp_read_data;
        /* test data end */

        osMutexAcquire(env_mutexId, osWaitForever); /* 获取环境数据互斥锁 */
        ret = SHT_BH1750_ReadData(&app_env_data);
        if (ret != 0)
        {
            MY_LOGI(TAG, "sht30_bh1750 Read Data failed!\r\n");
            return;
        }
        
        update_ctrl_status(MAX_HUM_ONE, MIN_LUX, 80, &app_env_data, &app_env_ctrl_status);
#if DEBUG
        MY_LOGI(TAG, "Lux Value   ->  %.2f\n", app_env_data.Lux);
        MY_LOGI(TAG, "Humidity    ->  %.2f\n", app_env_data.Humidity);
        MY_LOGI(TAG, "Temperature ->  %.2f\n", app_env_data.Temperature);
#endif

        osMutexRelease(env_mutexId); /* 释放环境数据互斥锁 */

        osDelay(TASK_DELAY_1S);
    }
}
