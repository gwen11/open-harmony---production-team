#include "app_log.h"
#include <stdio.h>
#include <string.h>
#include "stdarg.h"
#include "mutex.h"

/**
 * @brief my_log_printf
 * @note  自定义的日志输出函数
 * @param tag
 * @param level
 * @param format
 * @param ...
 */
void my_log_printf(const char *tag, my_log_level_t level, const char *format, ...)
{
    // 获取互斥锁
    osMutexAcquire(log_mutexId, osWaitForever);

    // 输出日志级别和标签
    printf("[%s]", tag);
    switch (level)
    {
    case MY_LOG_ERROR:
        printf("[E]");
        break;
    case MY_LOG_WARN:
        printf("[W]");
        break;
    case MY_LOG_INFO:
        printf("[I]");
        break;
    case MY_LOG_DEBUG:
        printf("[D]");
        break;
    case MY_LOG_VERBOSE:
        printf("[V]");
        break;
    default:
        break;
    }

    // 输出格式化字符串
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);

    // 释放互斥锁
    osMutexRelease(log_mutexId);
}
