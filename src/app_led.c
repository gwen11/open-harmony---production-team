#include "app_led.h"
#include "app_log.h"
#include "cmsis_os2.h"
#include "ohos_init.h"

#define LED_GPIO 5

const static char *TAG = "app_led.c";

/**
 * @brief LedInit
 *
 * @param uint8_t led_gpio
 */
void led_init(uint8_t led_gpio)
{
    // init gpio of LED
    IoTGpioInit(led_gpio);
    // set gpio to putong io
    IoTGpioSetFunc(led_gpio, IOT_GPIO_FUNC_GPIO_5_GPIO);
    // set GPIO_2 is output mode
    IoTGpioSetDir(led_gpio, IOT_GPIO_DIR_OUT);

    MY_LOGI(TAG,"----- LedGpioInit success! -----\r\n");
}

/**
 * @brief led_ctrl
 *
 * @param uint8_t led_gpio
 * @param uint8_t ledSta
 * @param uint16_t delay_t 10ms
 */
void led_ctrl(uint8_t led_gpio,bool ledSta,uint16_t delay_t)
{
    switch (ledSta)
    {
    case 1:
        // set GPIO_2 output high levels to turn on LED
        IoTGpioSetOutputVal(led_gpio, 1);
        MY_LOGI(TAG,"LED state: ON! %d\n",ledSta);
        if(delay_t == 0)
        {
            break;
        }
        else
        {
            osDelay(delay_t);
        }
        break;
    case 0:
        // set GPIO_2 output low levels to turn off LED
        IoTGpioSetOutputVal(led_gpio, 0);
        MY_LOGI(TAG,"LED state: OFF! %d\n",ledSta);
                if(delay_t == 0)
        {
            break;
        }
        else
        {
            osDelay(delay_t);
        }
        break;
    default:
        break;
    }
}

/**
 * @brief led_test_task
 *
 */
void led_test_task(void)
{
    led_init(LED_GPIO);
    while (1)
    {
        led_ctrl(LED_GPIO,1,500);
        for (uint8_t i = 0; i < 10; i++)
        {
            if(i%2 == 0)
            {
                led_ctrl(LED_GPIO,0,50);
            }
            else
            {
                led_ctrl(LED_GPIO,1,50);
            }
        }
        led_ctrl(LED_GPIO,0,500);
    }
}
