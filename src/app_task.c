#include "app_task.h"
#include "app_log.h"
#include "cmsis_os2.h"
#include "ohos_init.h"
#include "app_env.h"

const static char *TAG = "app_task.c";

#define APP_DELAY_1S 100 /* os tick is 10ms */
#define DEBUG 1


/**
 * @brief ctrl_priority
 *
 */
static void ctrl_priority(env_ctrl_status *ctrl)
{
    if(ctrl->flag_priority != 0)
    {
        ctrl->cloud_time--;
        if(ctrl->cloud_time == 0)
        {
            ctrl->flag_priority = 0;
        }
    }
}

/**
 * @brief Callback for Timer1 triggering
 *
 */
static void Timer1Callback(void)
{
    /* 定时器回调为了保证准确性，尽量不要在这里调用含Mutex的内容 */
    ctrl_priority(&app_env_ctrl_status);
#if DEBUG
    printf("[app_task.c][I]""Timer1Callback! cloud_time: %d\n",app_env_ctrl_status.cloud_time);
#endif
}

/**
 * @brief app_task
 */
void app_task(void)
{
    MY_LOGI(TAG, "Start Device Init!\r\n");
    /* init start */

    osTimerId_t id1;
    uint32_t timerDelay;
    osStatus_t status;
    id1 = osTimerNew(Timer1Callback, osTimerPeriodic, NULL, NULL);
    if (id1 != NULL)
    {
        // Hi3861 1U=10ms,100U=1S
        timerDelay = 100U;

        status = osTimerStart(id1, timerDelay);
        if (status != osOK)
        {
            printf("Failed to start Timer1!\n");
        }
    }

    uint16_t task_count = 0;

    /* init start */
    MY_LOGI(TAG, "Start Run app_task\r\n");
    while (1)
    {
        // MY_LOGI(TAG, "This task is test! %d\r\n", task_count++);
        osDelay(APP_DELAY_1S);
    }
}
