#ifndef __APP_LOG_H__
#define __APP_LOG_H__

// 定义日志级别枚举
typedef enum
{
    MY_LOG_NONE = 0,
    MY_LOG_ERROR,
    MY_LOG_WARN,
    MY_LOG_INFO,
    MY_LOG_DEBUG,
    MY_LOG_VERBOSE
} my_log_level_t;

void my_log_printf(const char *tag, my_log_level_t level, const char *format, ...);

#define MY_LOGI(tag, format, ...) my_log_printf(tag, MY_LOG_INFO, format, ##__VA_ARGS__)

#endif
