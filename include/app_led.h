#ifndef __APP_LED_H__
#define __APP_LED_H__

#include "stdbool.h"
#include "stdint.h"
#include "iot_gpio_ex.h"
#include "iot_gpio.h"

void led_test_task(void);
void led_ctrl(uint8_t led_gpio,bool ledSta,uint16_t delay_t);
void led_init(uint8_t led_gpio);

#endif