#ifndef __APP_OLED_H__
#define __APP_OLED_H__

#define OLED_NAME_GPIO_13  13//0
#define OLED_NAME_GPIO_14  14//1
#define OLED_IIC_IDX_0     0//1
#define OLED_IIC_BAUDRATE  200000
#define SSD1306_I2C_IDX     0//1
#define SSD1306_I2C_ADDR    (0x3C <<1)

void oled_test_task(void);

#endif


