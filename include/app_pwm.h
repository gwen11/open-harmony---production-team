#ifndef __APP_PWM_H__
#define __APP_PWM_H__

void pwmIO_init(uint8_t pwm_gpio,uint8_t pwm_gpio_func);
void pwm_ctrl(uint8_t pwm_gpio,uint8_t duty);
void pwm_test_task(void);

#endif


