#ifndef __APP_ENV_H__
#define __APP_ENV_H__

typedef struct app_env_ctrl_status
{
    uint16_t cloud_time;/* 云控制时间 */
    int flag_priority;  /* 控制标志优先级 */
    int fan;            /* 风扇控制状态 */
    int light;          /* 灯光控制状态 */
} env_ctrl_status;

typedef struct app_env_data
{
    float Lux;         // 光照强度
    float Humidity;    // 湿度
    float Temperature; // 温度
} env_data;

extern env_ctrl_status app_env_ctrl_status;
extern env_data app_env_data;

#define HYSTERESIS 2    /* 滞后控制 */
#define MIN_LUX 20      /* 光照阈值 */
#define MAX_HUM_ONE 60  /* 湿度阈值 */
#define MAX_TEMP_ONE 20 /* 温度阈值 */

void env_task(void);

#endif
