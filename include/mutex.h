#ifndef __MUTEX_H__
#define __MUTEX_H__

#include "cmsis_os2.h"
#include "ohos_init.h"

extern osMutexId_t log_mutexId;
extern osMutexId_t env_mutexId;
extern osMutexId_t ctrl_mutexId;

void create_mutex(void);

#endif
