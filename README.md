# OpenHarmony-生产大队

# 介绍
实验室环境监测项目是通过使用传感器和监测设备对实验室环境进行实时监测，管理员可以获得准确的环境数据，并能够迅速响应任何异常情况，确保实验室环境的稳定与安全。这样可以提高实验室工作效率、降低实验失败的风险，并为科研工作提供可靠的实验结果。    
  
  
## 实物演示
1. 硬件设备  
![实物](pic/实物.jpg)
2. 华为云设备监控  
![华为云](pic/华为云.jpg)
3. 命令下达  
![命令下达](pic/命令下达.jpg)

## 测试
```
/* use code module start */
#define LED_STUDY_TASK 0
#define PWM_STUDY_TASK 0
#define CLOUD_STUDY_TASK 1
#define ENV_STUDY_TASK 1
#define OLED_STUDY_TASK 1
#define APP_MAIN_TASK 1
/* use code module end */
```
